FROM alpine:3.17

RUN adduser --home /home/app --disabled-password app
USER app

ENV HOME /home/app
WORKDIR $HOME

COPY --chown=app:app ./gtbot $HOME
RUN chmod +x $HOME/gtbot

ENTRYPOINT ["/home/app/gtbot"]
