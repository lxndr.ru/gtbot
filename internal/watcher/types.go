package watcher

// Entry is a found match
type Entry struct {
	ChatID     int64
	PlayerName string
	Game       string
	ServerIP   string
	ServerName string
}
