package gametracker

import (
	"net/http"
	"regexp"
	"strings"

	"github.com/PuerkitoBio/goquery"
)

var (
	gameCodeRe = regexp.MustCompile(`^/search/(\w+)/`)
	serverIPRe = regexp.MustCompile(`\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}:\d{1,5}`)
)

func normalizePlayerName(name string) string {
	return strings.Trim(name, " \t\n\r")
}

func extractGameCode(iconHref string) string {
	m := gameCodeRe.FindStringSubmatch(iconHref)

	if len(m) == 2 {
		return m[1]
	}

	return ""
}

func extractServerIP(href string) string {
	return serverIPRe.FindString(href)
}

func makeRequest(url string) (*goquery.Document, error) {
	res, err := http.Get(url)

	if err != nil {
		return nil, err
	}

	defer res.Body.Close()

	return goquery.NewDocumentFromReader(res.Body)
}
