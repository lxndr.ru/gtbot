all: build

build: gtbot

gtbot: cmd/gtbot.go
		go build cmd/gtbot.go

clean:
		rm -rf gtbot

.PHONY: all build clean
