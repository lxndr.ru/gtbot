module gitlab.com/lxndr.ru/gtbot

go 1.22

require (
	github.com/PuerkitoBio/goquery v1.9.2
	github.com/go-telegram-bot-api/telegram-bot-api/v5 v5.5.1
	github.com/golang/glog v1.2.2
	github.com/joho/godotenv v1.5.1
	github.com/redis/go-redis/v9 v9.6.1
)

require (
	github.com/andybalholm/cascadia v1.3.2 // indirect
	github.com/cespare/xxhash/v2 v2.3.0 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	golang.org/x/net v0.28.0 // indirect
)
