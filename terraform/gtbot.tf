data "docker_registry_image" "gtbot" {
  name = "registry.gitlab.com/lxndr.ru/gtbot:latest"
}

resource "docker_service" "gtbot" {
  name = "lxndrru_gtbot"

  labels {
    label = "com.docker.stack.namespace"
    value = "lxndrru"
  }

  task_spec {
    networks = [data.docker_network.lxndrru.id]

    container_spec {
      image = "${data.docker_registry_image.gtbot.name}@${data.docker_registry_image.gtbot.sha256_digest}"

      labels {
        label = "com.docker.stack.namespace"
        value = "lxndrru"
      }

      env   = var.gtbot_env
    }
  }
}
