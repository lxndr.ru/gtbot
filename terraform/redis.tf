data "docker_registry_image" "redis" {
  name = "redis:latest"
}

resource "docker_service" "gtbot-redis" {
  name = "lxndrru_gtbot-redis"

  labels {
    label = "com.docker.stack.namespace"
    value = "lxndrru"
  }

  task_spec {
    networks = [data.docker_network.lxndrru.id]

    container_spec {
      image    = "${data.docker_registry_image.redis.name}@${data.docker_registry_image.redis.sha256_digest}"
      hostname = "gtbot-redis"

      mounts {
        type      = "volume"
        read_only = false
        source    = "lxndrru_gtbot-data"
        target    = "/data"
      }

      healthcheck {
        test = ["CMD", "redis-cli", "ping"]
      }

      labels {
        label = "com.docker.stack.namespace"
        value = "lxndrru"
      }
    }
  }
}
